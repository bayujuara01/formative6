# formative6

### Nexsoft Fun Coding Bootcamp Batch 7 Formative Day 5

## Simple Contact App


## How to run
### Windows
1. Just execute 'compileAndRun.bat'
2. Use Command prompt or terminal
```
java Main.java
```

### Linux
1. Use terminal
```
java Main.java
```

## Running program
1. Menu (user input start with '>')
```
====Simple Contact App====
1. Show contact
2. Add Contact
3. Delete Contact
4. Exit
Choice [1/2/3/4] : >
```

2. Show all contact
```
Contact List :
ID : 1
Name : Bayu Seno Ariefyanto
Gender : Male

ID : 2
Name : Denny Caknan
Gender : Male
```

3. Add new contact (user input start with '>')
```
====Add New Contact====
Input Name : > Bayu Seno Ariefyanto
Gender  :
1. Male
2. Female
Choice [1/2]  : > 1
```

4. Delete contact by id
```
====Delete Contact====
Contact ID [1/2/etc] : > 2
Contact with id : 2, deleted successfully
```