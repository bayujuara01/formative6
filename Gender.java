public enum Gender {
    MALE("Male"),
    FEMALE("Female"),
    UNKNOWN("Unknown");

    private final String name; 

    private Gender(String name) {
        this.name = name;
    }

    static Gender setValue(int choice) {
        switch (choice) {
            case 1:
                return Gender.MALE;
            case 2: 
                return Gender.FEMALE;
            default:
                return Gender.UNKNOWN;
        }
    }

    @Override
    public String toString() {
        return this.name;
    }
}
