import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Main {

    public static Logger logger = Logger.getLogger(Main.class.getName());

    private Scanner scanner;
    private int menuSelection;
    private Contact contact;

    public Main() {
        scanner = new Scanner(System.in);
        contact = new Contact();
    }

    public void run() throws Exception {
        do {
            showMenu();
            try {
                menuSelection = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                menuSelection = 0;
            }

            switch (menuSelection) {
                case 1:
                    clearScreen();       
                    print(contact.getAll());
                    break;
                case 2:
                    String name;
                    int choice;
                    clearScreen();
                    println("====Add New Contact====");
                    print("Input Name : ");
                    name = scanner.nextLine();
                    println("Gender  : ");
                    print("1. Male\n2. Female\nChoice [1/2]  : ");

                    try {
                        choice = Integer.parseInt(scanner.nextLine());
                    } catch (NumberFormatException e) {
                        choice = 0;
                    }

                    if (contact.add(name, choice)) {
                        println("Input new contact successfully.");
                    } else {
                        println("Failed input new contact.");
                    }

                    break;
                case 3:
                    int id = 0;
                    clearScreen();
                    System.out.println("====Delete Contact====");
                    System.out.print("Contact ID [1/2/etc] : ");

                    try {
                        id = Integer.parseInt(scanner.nextLine());
                    } catch (NumberFormatException e) {
                        id = 0;
                    }

                    boolean isDeleted = contact.deleteById(id);
                    if (isDeleted) {
                        println("Contact with id : " + id + ", deleted successfully");
                    } else {
                        println("Contact with id : " + id + ", failed to delete.");
                    }
                    break;

            }
            // clearScreen();

        } while (menuSelection != 4);
    }


    private void showMenu() {
        System.out.println("====Simple Contact App====");
        System.out.print("1. Show Contact\n2. Add Contact\n3. Delete Contact\n4. Exit\nChoice [1/2/3/4] : ");
    }

    private void print(String str) {
        System.out.print(str);
    }

    private void println(String str) {
        System.out.println(str);
    }

    private void clearScreen() {
        System.out.print("\033[H\033[2J");  
        System.out.flush();
    }

    public static void main(String[] args) {
        Main app = new Main();
        try {
            app.run();
        } catch (Exception e) {
            logger.log(Level.INFO, "Error : " + e.getMessage());
        }
    }
}
