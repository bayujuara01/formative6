import java.util.ArrayList;

public class Contact {
    private ArrayList<Person> persons = new ArrayList<>();
    private int lastId = 0;

    // View all person in contact
    public String getAll() {
        StringBuilder stringBuilder = new StringBuilder("Contact List : \n");

        if (persons.size() > 0) {
            for (Person person : persons) {
                stringBuilder.append(String.format("ID : %d\nName : %s\nGender : %s\n\n", person.getId(), person.getName(), person.getGender()));
            }
        } else {
            stringBuilder.append("Don't have any contact, please add new\n");
        }

        return stringBuilder.toString();
    }

    // Add person
    public boolean add(String name, int gender) {
        return persons.add(new Person(++lastId, name, Gender.setValue(gender)));
    }

    // Delete person
    public boolean deleteById(int id) {
        return persons.removeIf(persons -> persons.getId() == id);
    }
    
}